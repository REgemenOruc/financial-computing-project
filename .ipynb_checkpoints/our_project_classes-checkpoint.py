'''
* What are the important components in investment (types)? Investment amount, investment date, duration, minimum value etc.
* Risk is also a component but it is a component of a portfolio, not of an instrument itself.
* So, there are 2 separate classes for portfolio and investment instruments (including its subclasses)
'''

import datetime
from dateutil.relativedelta import relativedelta
import pandas as pd

class portfolio(object):
    #create a portfolio class to define exclusive portfolio functions depending on investor behaviour
    def __init__(self, stock, bond1, bond2):
        self.stock = stock
        self.bond1 = bond1
        self.bond2 = bond2

    def aggresive(self, stock):
        port = 1*stock
        return port

    def mixed(self, stock, bond1, bond2):
        port = 0.25 * stock + 0.75 * (0.5 * bond1 + 0.5 * bond2)
        return port

    def defensive(self, bond1, bond2):
        port = 0.5 * bond1 + 0.5 * bond2
        return port

class investment(object):
    def __init__(self, ini_date, amount):
        """ini_date as string in the format 'dd/mm/yyy' """
        dd = int(ini_date[0:2])
        mm = int(ini_date[3:5])
        yyyy = int(ini_date[6:10])

        try:
            self.ini_date = datetime.date(yyyy, mm, dd)
        except ValueError:
            print("not a valid ini_date")

        self.amount = amount

        print("Your investment date: %s. Your investment amount: %s" % (ini_date, amount))


class bond(investment):
    def __init__(self, ini_date, amount, term, int_rate, maturity):
        """term is an int, is the number of years added to ini_date """
        """maturity as string in the format 'dd/mm/yyy' """
        super().__init__(ini_date, amount)

        # self.end_date = self.ini_date.replace(self.ini_date.year + term)

        self.term = term
        self.int_rate = int_rate

        dd_c = int(maturity[0:2])
        mm_c = int(maturity[3:5])
        yyyy_c = int(maturity[6:10])
        self.maturity = datetime.date(yyyy_c, mm_c, dd_c)

    def price(self):
        """calculate the price of the bond in the current date"""
        inception_date = self.maturity.replace(self.maturity.year - self.term)

        if inception_date <= self.ini_date and inception_date <= self.maturity:
            t = (self.maturity - self.ini_date).days

            b_price = ((1 + self.int_rate / 365) ** (-1 * t / 365))
            return b_price, t
        else:
            return print("the compounded date must be within %s and %s" % (self.ini_date, self.maturity))

    # def term_structure()


class stock(investment):
    def __init__(self, ini_date, amount, end_date, ini_price, end_price):
        super().__init__(ini_date, amount)

        jj = int(end_date[0:2])
        ss = int(end_date[3:5])
        aaaa = int(end_date[6:10])

        try:
            self.end_date = datetime.date(aaaa, ss, jj)
        except ValueError:
            print("The end_date not valid")

        self.ini_price = ini_price
        self.end_price = end_price

    def rtrn(self):
        stock_rtrn = self.amount * (1 + (self.end_price - self.ini_price) / self.ini_price)
        return stock_rtrn