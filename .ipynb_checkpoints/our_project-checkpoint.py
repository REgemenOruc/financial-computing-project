import matplotlib.pyplot as plt
import os
import datetime
import pandas as pd
import scipy.stats

'''
3 types of investors: Aggresive, defensive and mixed
2 types of investments: Stocks and bonds
'''

### Part I ###

'''
variables = Our inv. term, our inv. amount, min. price, min. term, int. rate

* Short term bonds: 2 years, 250$, 0.015
* Long term bonds: 5 years, 1000$, 0.03
* compounded annually

* Q: Make a plot of the evolution of min. inv. over 50 years.
* We have to think of a loop for both and plot them together

ST: 250*(1+0.015)**2 loop (50/2) times
LT: 1000*(1+0.03)**5 loop (50/5) times

'''

# This part shows how short term bond minimum investment requirement evolves over 50 years.

shortterm = []
styears = []
stminval = 250
stint = 0.015
stdur = 2
ini_year = datetime.date.today()

for i in range(51):
    if i % stdur==0:
        shortterm.append((stminval * ((1+stint) ** i)))
        styears.append(ini_year.year + i)
print(shortterm)
print(styears)

# This part shows how long term bond minimum investment requirement evolves over 50 years.

longterm = []
ltyears = []
ltminval = 1000
ltint = 0.03
ltdur = 5

for i in range(51):
    if i % ltdur == 0:
        longterm.append((ltminval * ((1+ltint) ** i)))
        ltyears.append(ini_year.year + i)
print(longterm)
print(ltyears)

# Now, it is time o plot and export our results:

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(6,11))
fig.suptitle('Minimum investment prices of short term and long term bonds')
ax1.plot(styears, shortterm, label="Date", color='tab:blue', marker="o")
ax1.set_title("Short term")
ax1.grid(True)
ax1.legend(["2 years"])
ax1.set_xlabel("Year")
ax1.set_ylabel("Minimum Investment Price")
ax2.plot(ltyears, longterm, label="Date", color='tab:orange', marker="o")
ax2.set_title("Long term")
ax2.grid(True)
ax2.legend(["5 years"])
ax2.set_xlabel("Year")
ax2.set_ylabel("Minimum Investment Price")
fig.savefig(os.path.abspath("../Results/min_inv_prices"), dpi=1000)

### Part II ###

'''
Criterias:

* Investment term
* Investment Amount
* Stock name
* The time bought(business days)
* It should be possible to (given the start and end date) to get the price of the stock and return on investment. 
** If a start and end date is not a business day take the closest possible business day before the given date.

'''

# Importing the data required from Yahoo Finance through yfinance library. As required, 'High' section is taken as the stock price.

import yfinance as yf

tickerstouse = ['FDX', 'GOOGL', 'XOM', 'KO', 'NOK', 'MS', 'IBM']

df = yf.download(tickerstouse, start='2016-01-09', end='2021-01-01')['High']
df = df.dropna()
print(df)

# start date might need a modification because of the last feature of criterias.

df.to_csv("../Data/Part_2_Tickers.csv", sep=";")

# And now, it is time to plot our data:

fig3 = plt.figure(3, figsize=(15,9))
plt.plot(df)
plt.xlabel('Year')
plt.ylabel('Price ($)')
plt.title('Price change in these stocks between 09/01/2016 and 01/01/2021')
plt.legend(tickerstouse)
plt.grid(True)
plt.show()
fig3.savefig(os.path.abspath('../Results/Stock_Prices.png'), dpi=1000)

# oh I forgot to calculate their returns...

### Part III ###

'''
5000$ budget

500 defensive investors;
* will only invest on bonds randomly (50-50). 
* repeated till budget<250$

500 aggresive investors;
* will only invest on stocks. A stock will be chosen randomly.
* First, pick a random stock. Then a random amount (even 0) depending on the budget.
* repeated till budget<100$

500 mixed investors;
* 25-75 of buying stocks or bonds.
* if bond, 50-50 in long and short 
* repeated till budget<250$

You are not gonna lose money. He asks you to make to invest all your money but keep some of it in your budget.

'''

from our_project_classes import investment, bond, portfolio, stock
import random

## Test of investment and bond classes ##

x = investment("01/02/2021", 1000)
x1 = investment("01/02/2021", 3000)
y = bond("01/03/2021", 1000, 5, 0.03, "01/03/2026")
y1 = bond("01/03/2021", 3000, 2, 0.05, "01/03/2023")

print(y.ini_date)
print(y.term)
print(y.amount)
print(y.int_rate)

print(x.amount)
print(x.ini_date)
print(x1.amount)
print(x1.ini_date)

print(y.price())
print(y1.price())

## Test of the portfolio class ##

    # Test with integers:

adana = portfolio(3680, 5290, 4363)
print(adana)
print(adana.bond1)
print(adana.bond2)
print(adana.stock)
print(adana.aggresive(3256))
print(adana.defensive(5290, 4363))
print(adana.mixed(3680, 5290, 4363))

    # Let's make things more complicated:
        #P.S. - Never try to run the code below with .compounded

hercai = portfolio((x1.amount), y.amount, y1.amount)
print("Hercai's stock amount: %s" % (hercai.stock))
print("Hercai's 1st bond amount: %s " % (str(hercai.bond1)))
print("Hercai's 2nd bond amount: %s" % (str(hercai.bond2)))
print("If you are aggressive, here is what you get: %s" % (str(hercai.aggresive(x1.amount))))
print("If you are defensive, here is what you get: %s" % (str(hercai.defensive(y.amount, y1.amount))))
print("If you are mixed, here is what you get: %s" % (str(hercai.mixed(x1.amount, y.amount, y1.amount))))

## Test of the stock class ##

turai = stock("11/03/2021", 1850, "01/05/2023", 3.78, 3.93)
print("Between %s and %s, your initial investment of %s has become %s. Congratulations!" % (turai.ini_date, turai.end_date, turai.amount, turai.rtrn()))


## Defensive investor ##

    #Here, it is important to note that Python cannot interpret float data as integer
    #This is the reason why our range is defined as integers.
    #In addition, random.sample is not used because the sample of 500 defensive investors would be more than our population.

# Creating random datasets for short term and long term investment amounts:

budget = 5000
#random.uniform(int(shortterm[0]), int(shortterm[-1])
#random.uniform(longterm[0], longterm[-1])
shortterm_sample = random.choices(range(int(shortterm[0]), int(shortterm[-1])), k=500)      #Do we need k? If we do, should we set it to 250
longtterm_sample = random.choices(range(int(longterm[0]), int(longterm[-1])), k=500)        #just because of the 50-50 situation?
print(shortterm_sample)
print(longtterm_sample)

# Creating a loop for only 1 investor:
'''
def_investors = []

while budget >= 250:
    budget -= def_investment.defensive(def_investment.bond1, def_investment.bond2)
    def_investors.append(budget)
    if def_investors[-1] < 250:
        def_investors.pop(-1)   #because the last value appearing in the list is always negative.
print(def_investors)
'''

'''

# loop:

def_investors = []

for i in enumerate(shortterm_sample):      #doesn't really matter since they are both 500
    def_investment = portfolio(random.choice(longtterm_sample), random.choice(shortterm_sample), random.choice(longtterm_sample))
    print(def_investment.bond1, def_investment.bond2)

for i in range(len(shortterm_sample) + 1):
    budget -= def_investment.defensive(int(def_investment.bond1), int(def_investment.bond2))
    def_investors.append(budget)
    if def_investors[-1] < 250:
        def_investors.pop(-1)   #because the last value appearing in the list is always negative.
    print(def_investors)
'''

'''
# 1st loop:

for i in enumerate(shortterm_sample):      #doesn't really matter since they are both 500
    def_investment = portfolio(random.choice(longtterm_sample), random.choice(shortterm_sample), random.choice(longtterm_sample))
    print(def_investment.bond1, def_investment.bond2)

#longterm_sample random choice in stock is just a temporary measure till randomizing stocks

# 2nd loop:

def_investors = []

while budget >= 250:
    budget -= def_investment.defensive(def_investment.bond1, def_investment.bond2)
    def_investors.append(budget)
    if def_investors[-1] < 250:
        def_investors.pop(-1)   #because the last value appearing in the list is always negative.
print(def_investors)
'''

# it gives the same numbers but we want them to be different...
# THE PROBLEM IS CERTAINLY AT THE 2ND LOOP

## Aggressive investor ##
import numpy as np

chosen_stocks = np.random.choice(500, replace=False)
print(df.iloc[chosen_stocks])
